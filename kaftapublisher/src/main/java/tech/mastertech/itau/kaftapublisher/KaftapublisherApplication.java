package tech.mastertech.itau.kaftapublisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaftapublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaftapublisherApplication.class, args);
	}

}
