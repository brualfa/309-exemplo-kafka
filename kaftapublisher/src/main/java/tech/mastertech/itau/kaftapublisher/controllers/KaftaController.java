package tech.mastertech.itau.kaftapublisher.controllers;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.kaftapublisher.dtos.Kafta;

@RestController
@RequestMapping("/kafta")
public class KaftaController {
	
	@Autowired
	private KafkaTemplate<String, String> template;
	
	@PostMapping
	public void criarEspeto(@RequestBody Kafta kafta) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		template.send(new ProducerRecord<String, String>("churras-quarta", "1", mapper.writeValueAsString(kafta)));
	}
}
