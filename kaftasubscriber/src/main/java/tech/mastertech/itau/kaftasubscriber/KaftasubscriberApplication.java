package tech.mastertech.itau.kaftasubscriber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaftasubscriberApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaftasubscriberApplication.class, args);
	}

}
