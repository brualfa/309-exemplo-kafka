package tech.mastertech.itau.kaftasubscriber.services;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KaftaService {
	
	@KafkaListener(id="churrasconsumer", topics="churras-quarta")
	public void comer(@Payload String str) {
		System.out.println(str);
	}
	
}
