package tech.mastertech.itau.kaftasubscriber.dtos;

public class Kafta {
	private double comprimento;
	private boolean temCebola;
	private String molho;
	
	public double getComprimento() {
		return comprimento;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public boolean isTemCebola() {
		return temCebola;
	}
	public void setTemCebola(boolean cebola) {
		this.temCebola = cebola;
	}
	public String getMolho() {
		return molho;
	}
	public void setMolho(String molho) {
		this.molho = molho;
	}
	
	public String toString() {
		return "Kafta de " + comprimento + "cm e molho " + molho + " com cebola";
	}
	
}
